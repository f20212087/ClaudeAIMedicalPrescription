from image_processor import ImageProcessor
from user_interface import UserInterface

class Main:
    def __init__(self):
        self.api_key = None
        self.image_path = "medical1.jpg"

    def run(self):
        # Get API key from user
        if not self.api_key:
            self.api_key = UserInterface.get_api_key()

        image_processor = ImageProcessor(self.api_key)

        # Encode the image to base64
        image_data_base64 = image_processor.encode_image_to_base64(self.image_path)

        # Specify prompt or other parameters as needed
        prompt = "Short Description"

        # Send the image to the Anthropics API and retrieve the response
        response_text = image_processor.image_question(image_data_base64, prompt)

        # Print the response
        print("Response:", response_text)


if __name__ == "__main__":
    main = Main()
    main.run()


#api_key = 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